<?php
//1-OOP-Classes
class Task {
//property, public: allt utan þennan clasa hafa aðgang.
 public $description;
 //methods
 public funxtion __construct($description)
 	{
 		//setur $task sem $description
 		$this->description = $description
 	}

}

$task = new Task('Learn OOP');
//til að fá aðgang að description
var_dump($task->description)



//2-OOP-Getters-and-Setters   3-OOP-Encapsulation
class Person {
	//public: allt utan þennan clasa hafa aðgang.
	//public = $name
	//public = $age

	//til að komast frammhja þvi að $john->age = 15; fari í gegn, private kemst ekki í gegn nema fara í gegnum getAge og setAge.
	private = $name
	private = $age



	public function __construct($name)
	{
		$this->name = $name;
	}
	//nær í aldur, Getters
	public function getAge()
	{
		//skilar aldur og skilar aldur í dögum.
		return $this->age * 365;
	}



	//Skoðar aldur, Setters
	public function setAge($age)
	{
		//ef aldur er minni en 18
		if($age < 18)
		{
			//prentar villu
			throw new Exception("villa");
			
		}
		$this->age = $age
	}
}
//setur $name = John Doe
$john = new Person('John Doe');

//setur aldur sem 30
//$john->setAge(30);

//harðkóðað age, fer frammhjá setAge()
$john->age = 15;

//teljari, hækkar aldur
$john->age++;

//prentar út $john, nafnið og aldur.
var_dump($john)

//til að ná í aldur aftur.
var_dump($john->getAge());



//3-OOP-Encapsulation
class lightSwitch {
	public function on()
	{

	}
	public function off()
	{

	}
	//bara hægt að nota inní þessum clasa
	private function connect()
	{

	}
	//bara hægt að nota inní þessum clasa
	private function activate()
	{

	}
}
$switch = new lightSwitch;
$switch->connect();




//4-OOP-heritance

class Mother {

	public function getEyeCount()
	{
		return 2;
	}
}
class Child extends Mother {

}
?>