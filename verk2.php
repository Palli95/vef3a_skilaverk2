1. Myndbönd

2.Af hverju ættir þú að nota OOP í PHP? Hvenær væri það hugsanlega heppilegt og
hvenær ekki. Hver er munurinn á procedural programming og OOP. Komdu með dæmi
og rökstuðning.
OOP er minni kóði, gott að nota OOP þegar maður eg kominn með stór verkefni, hægt að nota sömu method aftur og aftur.



3. Hver er munurinn á public og private members þegar kemur að:
a) property -> public hægt að nota fyrir utan clasan, private bara innanum clasan.
b) methods -> private methods er oft notað til að brjóta niður stórt verkefni í minni búta, private bara hægt að nota innanum clasan en public fyrir utan

4. Hvað er namespaces og Autoloading?
	namespaces: Flokkar niður skrár til að minnka kóðan.
	autoloading: tengir skrárnar saman, minnkar kóðan.

5. Útskýrðu Interfaces.
	Object Interfaces leyfa þér að búa til kóða sem tilgreinir hvaða aðferðir flokki verður framkvæmd,
	 án þess að þurfa að skilgreina hvernig þessar aðferðir eru meðhöndlaðar.
	 verða alltaf að vera public svo hægt að að nota þær.


6. Búðu til með php class sem heitir Bók 

<?php
class bok {

	public $titill;
	public $verd;



	public function setTitle($titill)
	{
		$this->titill = $titill;
	}
	public function setPrice($verd)
	{
		$this->verd = $verd;
	}
	public function getPrice()
	{
		return $this->verd;
	}
	public function getTitle()
	{
		return $this->titill;
	}

	//abstract protected function setPublisher();

}

$bokin = new Bok('bok1');
$bokin->setPrice(1500);



?>
7.Notaðu Bók class og gerðu eftirfarandi:
búðu til þrjú object; $efnafraedi, $staerdfraedi og $islenska.
Gefðu þessum objectum titil og verð
<?php
$efnafraedi = new Bok('efnafraedibok');
$efnafraedi->setTitle('efnafraedibok');
$efnafraedi->setPrice(6500);

$staerdfraedi = new Bok('staerdfraedibok');
$staerdfraedi->setTitle('staerdfraedibok');
$staerdfraedi->setPrice(3500);

$islenska = new Bok('islenskubok');
$islenska->setTitle('islenskubok');

$islenska->setPrice(2000);
var_dump($bokin, $efnafraedi, $staerdfraedi, $islenska);

?>
8. búðu til class sem erfir (extends) Bók class. Bættu við eigindinu publisher og
tveimur aðferðum getPublisher og setPublisher. Birtu bókaupplýsingarnar.

<?php

class bokerfir extends bok {

public $publisher;



	public function setPublisher($publisher)
	{
		$this->publisher = $publisher;
	}
	public function getPublisher()
	{
		return $this->publisher;
	}

}
var_dump($bokin, $efnafraedi, $staerdfraedi, $islenska);


?>

9. Búðu til class með OOP aðferðinni sem heitir User sem geymir gögn um notanda.
User þarf að hafa eftirfarandi:
Properties; email og password.
Methods: setPassword(), getPassword(), setEmail(), getEmail(),

<?php

class users {
	public $email;
	public $password;

	public function setPassword($password)
	{
		$this->password = $password;
	}
	public function getPassword()
	{
		return $this->password;
	}


	public function setEmail($email)
	{
		$this->email = $email;
	}
	public function getEmail()
	{
		return $this->email;
	}


}
$user1 = new users();
$user1->setEmail('email@email.com');
$user1->setPassword('emailpassword');
$user2 = new users();
$user2->setEmail('email2@email.com');
$user2->setPassword('emailpassword2');
var_dump($user1,$user2);

?>